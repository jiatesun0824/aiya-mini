import Taro from '@tarojs/taro'

const TokenKey: string = 'Authorization'

export const setToken = (token: string) => { Taro.setStorageSync(TokenKey, token); return token;  }

export const getToken = (): string => Taro.getStorageSync(TokenKey) || ''

export const removeToken = () => { Taro.removeStorageSync(TokenKey); return '';}