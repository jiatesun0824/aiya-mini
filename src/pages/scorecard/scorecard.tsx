import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import './scorecard.scss'
const scorecard_qr_icon: string = 'https://aiyouya.oss-cn-shenzhen.aliyuncs.com/wechat/user/20201122153337.jpg'

type PageStateProps = {
  store: {
    commonStore: {
      userInfoGet: Function,
      userInfo: {
        ytRecordList: Array<any>,
        recheckRecordList: Array<any>
        wxUser: {
          isUpdated: number,
          score: number
        }
      }
    },
    loginStore: {
      loginStatus: Promise<any>
    }
  }
  // store: {
  //   counterStore: {
  //     counter: number,
  //     increment: Function,
  //     decrement: Function,
  //     incrementAsync: Function
  //   }
  // }
}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Scorecard { props: IProps }

@inject('store')
@observer
class Scorecard extends Component {
  public state = {
    score: 0
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { 
    this.showInit()
  }

  componentDidHide () { } 

  showInit() {
    this.userInfoGet()
  }

  userInfoGet = async () => {
    const { store: { commonStore, loginStore } } = this.props
    const flag = await loginStore.loginStatus
    if (flag) {
      await commonStore.userInfoGet()
      this.setState({ score: commonStore.userInfo.wxUser.score || 0})
    }
  }

  render () {
    const { score } = this.state
    return (
      <View className='scorecard'>
        <View className='scorecard-header'>
          <View className='header-num'>{score}</View>
          <View className='header-text'>当前总积分</View>
        </View>
        <View className='scorecard-rule'>
          <View className='rule-title'>积分规则</View>
          <View className='rule-box'>
            <View className='rule-item'>
              <View className='item-num'>100分</View>
              <View className='item-text'>完善基本信息</View>
            </View>
            <View className='rule-line'></View>
            <View className='rule-item'>
              <View className='item-num'>20分</View>
              <View className='item-text'>每增加一副牙套</View>
            </View>
            <View className='rule-line'></View>
            <View className='rule-item'>
              <View className='item-num'>20分</View>
              <View className='item-text'>记录每次复诊</View>
            </View>
          </View>
        </View>
        <View className='scorecard-notice'>
          <View className='notice-icon'></View>
          积分兑换，目前以人工手段进行积分兑换和核销
        </View>
        <View className='scorecard-require'>
          <View className='require-title'>兑换要求</View>
          <View className='require-item'><View className='item-index'>1</View>需提供牙套袋子信息，以验真实牙套副数；</View>
          <View className='require-item'><View className='item-index'>2</View>复诊信息以10次为上限；</View>
          <View className='require-qrcode'>
            <Image src={scorecard_qr_icon} className='qrcode'></Image>
          </View>
          <View className='require-explain'>联系小优申请兑换礼品和核销积分</View>
        </View>
      </View>
    )
  }
}

export default Scorecard
