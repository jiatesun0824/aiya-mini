import React, { Component } from 'react'
import { View, Picker} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import { recordApi } from '../../services'
import Taro from '@tarojs/taro'

import './otherCourse.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface OtherCourse { props: IProps }

interface IState {
  regionData: Array<any>
  otherPersonMindRecordList: Array<any>
  cityData: any
}

@inject('store')
@observer
class OtherCourse extends Component {
  public state: IState = {
    regionData: [],
    otherPersonMindRecordList: [],
    cityData: {
      list: [
        {name: '全部', id: 0},
        {name: '广东省深圳市', id: 1},
        {name: '广东省广州市', id: 2},
        {name: '广东省东莞市', id: 3},
        {name: '广东省珠海市', id: 4},
        {name: '广东省佛山市', id: 5},
        {name: '广东省中山市', id: 6},
        {name: '广东省惠州市', id: 7},
      ],
      type: {name: '请选择', id: 0}
    }
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onChange = async (e: any, type: string) => {
    let data = this.state[type]
    switch (type) {
      case 'cityData':
        data.type = data.list[e.detail.value]
        break;
      default:
        data = e.detail.value
    }
    this.setState({ [type]: data })
    if (type === 'cityData') {
      const city = data.type.name === '全部' ? '' : data.type.name
      const res = await recordApi.otherPersonMindRecordsGet(Object.assign(city ? { city }: {}))
      this.setState({ otherPersonMindRecordList: res.data || []})
    }
  }

  willMountInit = async () => {
    const { data } = await recordApi.otherPersonMindRecordsGet()
    this.setState({ otherPersonMindRecordList: data})
  }

  routerToCourseHome(id: number) {
    Taro.navigateTo({ url: `/pages/courseHome/courseHome?id=${id}` })
  }

  render () {
    const {  otherPersonMindRecordList , cityData} = this.state
    console.log(otherPersonMindRecordList, 'otherPersonMindRecordList')
    return (
      <View className='other-course'>
        <View className='course-content'>
        <View className='selector-box'>
          <Picker mode='selector' range={cityData.list} rangeKey='name' value={cityData.type.name} onChange={e => {this.onChange(e, 'cityData')}}>
            <View className='selector-result'>
              <View className='result-left'>所在城市</View>
              <View className='result-right'>
                <View className='name'>{cityData.type.name}</View>
                <View className='icon'></View>
              </View>
            </View>
          </Picker>
        </View>
          {/* <View className='selector-box'>
            <Picker mode='region' value={regionData} customItem='全部' onChange={e => {this.onChange(e)}}>
              <View className='selector-result'>
                <View className='result-left'>所在城市</View>
                <View className='result-right'>
                  <View className='name'>
                  {regionData.length ? `${(regionData[0] || '')}-${(regionData[1] || '')}-${(regionData[2] || '')}` : '请选择'}
                  </View>
                  <View className='icon'></View>
                </View>
              </View>
            </Picker>
          </View> */}
          <View className='course-list'>
            {
              otherPersonMindRecordList.map((item, index) => 
                <View className='course-image' onClick={this.routerToCourseHome.bind(this, item.id)}>{index + 1}、{item.feel_content}</View>
              )
            }
          </View>
        </View>
      </View>
    )
  }
}

export default OtherCourse
