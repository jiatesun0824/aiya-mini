import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import classnames from 'classNames'

import Taro, { getCurrentInstance } from '@tarojs/taro'

import './bracesLog.scss'
import { recordApi } from '../../services'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BracesLog { props: IProps }

interface IState {
  isShare: boolean
  myDailyRecord: Array<any>
}

@inject('store')
@observer
class BracesLog extends Component {
  public state: IState = {
    isShare: false,
    myDailyRecord: []
  }

  componentWillMount () { 
    this.init()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { } 

  init() {
    this.getMyDailyRecord()
  }

  getMyDailyRecord = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    if (id) {
      const { data } = await recordApi.getYTRecordList({ openid: id})
      this.setState({ myDailyRecord: data })
    } else {
      const { data } = await recordApi.getMyDailyRecord()
      this.setState({ myDailyRecord: data.ytRecordList || [] })
    }
  }

  shareCheck = () => {
    this.setState({ isShare: !this.state.isShare }, () => {
      recordApi.allowShare({shareType: this.state.isShare ? 1 : 0})
    })
  }

  routerTo = (url: string) => {
    Taro.navigateTo({ url: `/pages/${url}/${url}` })
  }

  render () {
    const { isShare, myDailyRecord } = this.state
    return (
      <View className='bracesLog'>
        <View className='top-btn'>牙套日志</View>
        {
          myDailyRecord.length === 0 && <View className='error-data'>
            暂无牙套信息，请前往首页新增
          </View>
        }
        {
          myDailyRecord.map((item, index) => {
            return <View className={classnames('bracesLog-item', index % 2 === 0 && 'bracesLog-item-active')} key={index}>
              <Image src={item.img_1} className='item-image'></Image>
              <View className='item-info'>
                <View className='info-title'>第{index + 1}副牙套</View>
                <View className='info-content'>
                  {item.yt_space_content}
                </View>
                { index < myDailyRecord.length -1 && <View className='info-icon'></View> }
              </View>
            </View>
          })
        }
        {
          myDailyRecord.length > 0 && <View className='share-box'>
            <View className={classnames('share-check', isShare && 'share-check-active')}
              onClick={this.shareCheck}>
            </View>
            分享到牙套日志社区
          </View>
        }
        <View className='btn-box'>
          <View className='btn' onClick={this.routerTo.bind(this, 'community')}>看看别人<View className='btn-icon'></View></View>
        </View>
      </View>
    )
  }
}

export default BracesLog
