import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import { recordApi } from '../../services'

import './bracesShare.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BracesShare { props: IProps }

@inject('store')
@observer
class BracesShare extends Component {
  public state = {
    date: '',
    bracesCount: 1,
    bracesDaycount: 1,
    notesText: '',
    bracesImageUrl: '',
    bracesId: '',
    bracesContent: ''
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    this.setState({ bracesId: id })
    if (id) {
      const { data } = await recordApi.ytRecordGet({ id })
      const { img_1, feel, startDateStr, yt_number, day_count, yt_space_content} = data
      this.setState({
        date: startDateStr,
        bracesCount: yt_number,
        bracesDaycount: day_count,
        notesText: feel,
        bracesImageUrl: img_1,
        bracesId: id,
        bracesContent: yt_space_content
      })
    }
  }

  routerToOtherCourse() {
    Taro.navigateTo({ url: '/pages/otherCourse/otherCourse' })
  }

  navigateBack() {
    Taro.navigateBack()
  }

  render () {
    const { bracesContent, bracesImageUrl, bracesCount} = this.state
    return (
      <View className='braces-share'>
        <View className='braces-card'>
          <View className='card-title'>
            第{bracesCount}副牙套
          </View>
          <Image src={bracesImageUrl} className='card-image'></Image>
          <View className='card-explain'>
            {bracesContent}
          </View>
        </View>
        <View className='btn-box'>
          <View className='btn' onClick={this.navigateBack}>修正信息<View className='btn-icon'></View></View>
        </View>
        {/* <View className='content'>
          <View className='content-text'>
            &nbsp;&nbsp;&nbsp;&nbsp;{bracesContent}
          </View>
          <Image className='content-image' src={bracesImageUrl}></Image>
        </View>
        <View className='btn-box'>
          <View className='btn' onClick={this.navigateBack}>修正信息<View className='left-icon'></View></View>
          <View className='btn' onClick={this.routerToOtherCourse}>看看别人<View className='right-icon'></View></View>
        </View> */}
      </View>
    )
  }
}

export default BracesShare
