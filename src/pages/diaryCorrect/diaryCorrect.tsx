import React, { Component } from 'react'
import { View, Picker } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import Option from '../../component/option/option'
import { recordApi } from '../../services'

import './diaryCorrect.scss'

type PageStateProps = {
  store: {
    commonStore: {
      userInfoGet: Function,
      userInfo: {
        ytRecordList: Array<any>,
        recheckRecordList: Array<any>
        wxUser: {
          isUpdated: number
        }
      },
      mindRecordList: Array<any>
    },
    loginStore: {
      loginStatus: Promise<any>
    }
  }
}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface DiaryCorrect { props: IProps }

@inject('store')
@observer
class DiaryCorrect extends Component {
  public state = {
    beckoningDate: '',
    firstCureDate: '',
    confirmDate: '',
    firstProjectDate: '',
    startWearDate: '',
    diaryId: '',
    routeType: 0
  }

  componentWillMount () {
    this.willMountInit()
   }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { type } = router?.params!
    this.setState({routeType: type})
    const { data } = await recordApi.mindRecordListGet()
    if (data.length > 0) {
      const { confirmCorrectStr, firstCheckStr,firstCorrectPlanStr, startCorrectDateStr, startMindDateStr} =  data[0]
      this.setState({
        beckoningDate: startMindDateStr,
        firstCureDate: firstCheckStr,
        confirmDate: confirmCorrectStr,
        firstProjectDate: firstCorrectPlanStr,
        startWearDate: startCorrectDateStr,
        // diaryId: id
      })
    }
  }

  onChange = (e: any, type: string) => {
    let data = this.state[type]
    switch (type) {
      default:
        data = e.detail.value.split('-').join('/')
    }
    this.setState({ [type]: data })
  }

  routerToPerfectInformation = async () =>  {
    const { confirmDate, firstCureDate, firstProjectDate, startWearDate, beckoningDate, diaryId} = this.state
    if (confirmDate != '请选择' && firstCureDate != '请选择' && firstProjectDate != '请选择' && startWearDate != '请选择' &&
    beckoningDate != '请选择') {
      await recordApi.mindRecordAdd(Object.assign({
        "startMindDateStr": beckoningDate,
        "firstCheckStr": firstCureDate,
        "confirmCorrectStr": confirmDate,
        "firstCorrectPlanStr": firstProjectDate,
        "startCorrectDateStr": startWearDate,
      }, diaryId ? { id: diaryId } : {}))
      Taro.navigateTo({ url: `/pages/courseHome/courseHome?type=1`})
    } else {
      Taro.showToast({ title: '请填写完整信息', icon: 'none' }) 
    }
  }

  render () {
    const { beckoningDate, firstCureDate, confirmDate, firstProjectDate, startWearDate } = this.state
    return (
      <View className='diary-correct'>
        {/* 什么时候开始心动 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'beckoningDate')}}>
          <Option title='什么时候开始心动' result={beckoningDate || ''}></Option>
        </Picker>
        {/* 第一次求诊 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'firstCureDate')}}>
          <Option title='第一次求诊' result={firstCureDate}></Option>
        </Picker>
        {/* 确定需要做矫正 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'confirmDate')}}>
          <Option title='确定需要做矫正' result={confirmDate}></Option>
        </Picker>
        {/* 第一次看到矫正方案 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'firstProjectDate')}}>
          <Option title='第一次看到矫正方案' result={firstProjectDate}></Option>
        </Picker>
        {/* 什么时候开始初戴 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'startWearDate')}}>
          <Option title='什么时候开始初戴' result={startWearDate}></Option>
        </Picker>
        {/* 按钮 */}
        <View className='btn-box'>
          <View className='btn' onClick={this.routerToPerfectInformation}>下一步<View className='btn-icon'></View></View>
        </View>
      </View>
    )
  }
}

export default DiaryCorrect
