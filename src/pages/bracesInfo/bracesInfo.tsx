import React, { Component } from 'react'
import { View, Input, Image, Picker, Textarea } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import { recordApi } from '../../services'
import Option from '../../component/option/option'

import './bracesInfo.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface BracesInfo { props: IProps }

@inject('store')
@observer
class BracesInfo extends Component {
  public state = {
    date: '',
    bracesCount: 1,
    bracesDaycount: 1,
    notesText: '',
    bracesImageUrl: '',
    bracesId: ''
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    this.setState({ bracesId: id })
    if (id) {
      const { data } = await recordApi.ytRecordGet({ id })
      const { img_1, feel, startDateStr, yt_number, day_count} = data
      this.setState({
        date: startDateStr,
        bracesCount: yt_number,
        bracesDaycount: day_count,
        notesText: feel,
        bracesImageUrl: img_1,
        bracesId: id
      })
    }
  }

  onChange = (e, type: string) => {
    let data = this.state[type]
    const value = e.detail.value
    switch (type) {
      case 'bracesCount':
        data = value < 1 ? 1 : value
        break;
      case 'bracesDaycount':
        data = value < 1 ? 1 : value
        break;
      case 'date':
        data = value.split('-').join('/')
        break;
      default:
        data = value
    }
    this.setState({ [type]: data })
  }

  routerToBracesShare = async () => {
    const {bracesId,  bracesCount, bracesDaycount, bracesImageUrl, date, notesText} = this.state
    if (bracesImageUrl && date != '请选择' && notesText) {
      const { data } = await recordApi[ bracesId? 'ytRecordUpdate' :'ytRecordAdd'](Object.assign({
        yt_number: bracesCount,
        startDateStr:date,
        day_count: bracesDaycount,
        feel: notesText,
        img_1: bracesImageUrl
      }, bracesId ? { id: bracesId } : {}))
      Taro.navigateTo({ url: `/pages/bracesShare/bracesShare?id=${data.id}` })
    } else {
      Taro.showToast({ title: '请填写完整信息', icon: 'none' })
    }
  }

  chooseImage = async () => {
    const { tempFilePaths } = await Taro.chooseImage({ count: 1 })
    const { data } = await recordApi.uploadFileImage({ filePath: tempFilePaths[0] })
    this.setState({bracesImageUrl: data })
  }

  render () {
    const { date, bracesCount, notesText, bracesImageUrl, bracesDaycount } = this.state
    return (
      <View className='braces-info'>
        {/* 这是第几副 */}
        <Option title='这是第几副' result={bracesCount} type='number' blur={e => {this.onChange(e, 'bracesCount')}}></Option>
        {/* 开始初戴日期 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'date')}}>
          <Option title='开始初戴日期' result={date}></Option>
        </Picker>
        {/* 需要戴几天 */}
        <Option title='需要戴几天' result={bracesDaycount} type='number' blur={e => {this.onChange(e, 'bracesDaycount')}}></Option>
        {/* 记录下这副牙套的心里感受 */}
        <View className='notes-box'>
          <Textarea className='notes' placeholder='记录下这副牙套的心里感受...' value={notesText} onBlur={e => {this.onChange(e, 'notesText')}}></Textarea>
        </View>
        {/* 上传图片 */}
        <View className='upload-file-box'>
          { bracesImageUrl && <Image src={bracesImageUrl} className='file-image'></Image> }
          {
            bracesImageUrl ? <View className='edit-file-icon' onClick={this.chooseImage}>修改</View> : 
            <View className='add-file-icon' onClick={this.chooseImage}></View>
          }
        </View>
        {/* 按钮 */}
        <View className='btn-box'>
          <View className='braces-btn' onClick={this.routerToBracesShare}>
            下一步
            <View className='btn-icon'></View>
          </View>
        </View>
      </View>
    )
  }
}

export default BracesInfo
