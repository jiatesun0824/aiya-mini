import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import { recordApi } from '../../services'

import './examineRecord.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface ExamineRecord { props: IProps }

interface IState {
  examineRecord: any
}

@inject('store')
@observer
class ExamineRecord extends Component {
  public state: IState = {
    examineRecord: {}
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    if (id) {
      this.setState({ restoreRecordId: id})
      const { data } = await recordApi.reCheckRecordGet({ id })
      this.setState({ examineRecord: data})
    }
  }

  routerToOtherCourse() {
    Taro.navigateTo({ url: '/pages/otherCourse/otherCourse' })
  }

  navigateBack() {
    console.log('lai')
    Taro.navigateBack()
  }

  routerToExplain = () => {
    const { examineRecord } = this.state
    Taro.navigateTo({ url: `/pages/explain/explain?id=${examineRecord.check_type}`})
  }

  render () {
    const { space_content, imgUrl, check_number} = this.state.examineRecord
    console.log(this.state.examineRecord)
    return (
      <View className='examine-record'>
        <View className='examine-card'>
          <View className='card-title'>
            <View className='title-line'></View>
              第{check_number}次复诊
            <View className='title-line'></View>
          </View>
          <Image src={imgUrl} className='card-image'></Image>
          <View className='card-explain' onClick={this.routerToExplain}>
            {space_content}
          </View>
        </View>
        <View className='btn-box'>
          <View className='btn' onClick={this.navigateBack}>修正信息<View className='btn-icon'></View></View>
        </View>
        {/* <View className='content'>
          <View className='content-text' onClick={this.routerToExplain}>
            &nbsp;&nbsp;&nbsp;&nbsp; {space_content}
          </View>
          <View className='content-contrast'>这个复诊和上一次的对比</View>
          <View className='content-image'>
            <Image src={prev_img!} className='image-item'/>
            <Image src={imgUrl!} className='image-item'/>
          </View>          
        </View>
        <View className='btn-box'>
          <View className='btn' onClick={this.navigateBack}>修正信息<View className='left-icon'></View></View>
          <View className='btn' onClick={this.routerToOtherCourse}>看看别人<View className='right-icon'></View></View>
        </View> */}
      </View>
    )
  }
}

export default ExamineRecord
