import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import classNames from 'classNames'
import Taro from '@tarojs/taro'
import './doctorList.scss'
import doctor_qr_icon from '../../assets/images/doctor_qr_icon.png'
import { doctorApi } from '../../services'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface DoctorList { props: IProps }

interface IState  {
  doctorList: Array<any>
}

class DoctorList extends Component {
  public state: IState = {
    doctorList: [],
  }

  componentWillMount() { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit() {
    this.getDoctorsList()
  }

  getDoctorsList = async () => {
    const { data } = await doctorApi.getDoctorsList()
    this.setState({ doctorList: data })
  }

  routerTo(url: string, item: any) {
    Taro.navigateTo({url: `/pages/${url}/${url}?name=${item.name}`})
  }

  render () {
    const { doctorList } = this.state
    return (
      <View className='doctor-list'>
        <View className='doctor-box'>
          {
            doctorList.map((item, index) => {
              return <View className={classNames('doctor-item', index < doctorList.length - 1  && 'doctor-item-border')} key={index}>
                <Image src={item.photo} className='doctor-head'></Image>
                <View className='doctor-info'>
                  <View className='info-name'>{item.name}</View>
                  <View className='info-seniority'>{item.university_1}</View>
                </View>
                <View onClick={this.routerTo.bind(this, 'doctorHome', item)} className='doctor-btn'>了解更多<View className='btn-icon'></View></View>
              </View>
            })
          }
        </View>
        <View className='doctor-qr-code'>
          <View className='qr-code-explain'>将小优医生推荐给您的正畸医生，我们会为医生增加信息</View>
          <Image src={doctor_qr_icon} className='qr-code'></Image>
        </View>
      </View>
    )
  }
}

export default DoctorList
