import React, { Component } from 'react'
import { View, Picker, Input, Button } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { chooseInvoiceTitle } from '@tarojs/taro'
import Option from '../../component/option/option'
import './homeImport.scss'

import { commonApi } from '../../services'

type PageStateProps = {
  store: {
    commonStore: {
      userInfoGet: Function,
      userInfo: {
        ytRecordList: Array<any>,
        recheckRecordList: Array<any>
        wxUser: {
          isUpdated: number,
          city: string
          age: number
          doctor_name: string 
          fault_name: string
          fault_type: number
          yt_name: string
          yt_type: number
          sex: number
          total_yt_count: number,
          province: string,
          planEndDateStr: string
        }
      },
      teethDictData: Array<any>,
      diseaseDictData: Array<any>,
      visitDictData: Array<any>,
    },
    loginStore: {
      loginStatus: Promise<any>
    }
  }
}

interface HomeImport {
  props: PageStateProps;
}

enum RecordType {
  one =  '地包天',
  two = '天包地',
  three = '包着地'
}

enum SexType {
  one = '男',
  two = '女'
}

@inject('store')
@observer
class HomeImport extends Component {
  public state = {
    coverData: {
      cover: { name: '', id: 0 },
      list: [
        { name: '隐适美', id: 1 },
      ]
    },
    regionData: [],
    doctorName: '',
    sexData: {
      sex: { name: '请选择', id: 0 },
      list: [
        { name: '男', id: 1 },
        { name: '女', id: 2 },
      ]
    },
    birthday: '',
    endTime: '',
    // ageData: {
    //   age: 0,
    //   list: [1, 2, 3, 4, 5, 6, 7, 8]
    // },
    ageData: '',
    countData: '',
    typeData: {
      type: { name: '请选择', id: 0 },
      list: [
        { name: '地包天', id: 1 },
      ],
    },
    planEndData: ''
    // countData: {
    //   count: 0,
    //   list: [1, 2, 3, 4, 5, 6, 7, 8]
    // },
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = () => {
    // this.setBasicType()
    this.setDefauleData()
  }
  setBasicType = () => {
    const { store: { commonStore } } = this.props
    const { coverData, typeData} = this.state
    coverData.list = commonStore.teethDictData
    typeData.list = commonStore.diseaseDictData
    this.setState({ coverData, typeData })
  }

  setDefauleData = async () => {
    const { store: { commonStore } } = this.props
    const { coverData, typeData, sexData} = this.state
    coverData.list = commonStore.teethDictData
    typeData.list = commonStore.diseaseDictData
    if (commonStore.userInfo.wxUser.isUpdated=== 1) {
      const { city, province, age, doctor_name, fault_name, fault_type, yt_name, yt_type, sex, planEndDateStr,  total_yt_count} = commonStore.userInfo.wxUser
      coverData.cover = {name: yt_name, id: yt_type}
      typeData.type = {name: fault_name, id: fault_type}
      sexData.sex = {name: this.sexTypeConversion(sex || 1), id: sex || 1}
      console.log(city, age, doctor_name, fault_name, fault_type, sex, total_yt_count)
      this.setState({
        coverData,
        regionData: [province, city, ''],
        doctorName: doctor_name,
        sexData,
        ageData: age,
        countData: total_yt_count,
        typeData,
        endTime: planEndDateStr
      })
    }
  }

  recordTypeConversion(type: number): string {
    const { typeData } = this.state
    const data: any = typeData.list.find((item: any) => item.id == type)
    return data.name
  }

  sexTypeConversion(type: number) {
    let data = ''
    switch(type) {
      case 1: data = SexType.one; break;
      case 2: data = SexType.two; break;
    }
    return data
  }

  onChange = (e, type) => {
    let data = this.state[type]
    switch (type) {
      case 'coverData':
        data.cover = data.list[e.detail.value]
        break
      case 'sexData':
        data.sex = data.list[e.detail.value]
        break
      // case 'ageData':
      //   data.age = data.list[e.detail.value]
      //   break
      case 'typeData':
        data.type = data.list[e.detail.value]
        break
      // case 'countData':
      //   data.count = data.list[e.detail.value]
      //   break
      case 'birthday':
        data = e.detail.value.split('-').join('/')
        break
      case 'endTime':
        data = e.detail.value.split('-').join('/')
        break
      default:
        data = e.detail.value
    }
    this.setState({ [type]: data })
  }


  personalPageGenerate = async (userInfo: any) => {
    const { ageData, countData, coverData, doctorName, regionData, sexData, typeData, endTime} = this.state
    const newRegionData = regionData.slice().map((item: any) => item == '全部' ? '' : item)
    console.log(ageData + 'a', countData + 'b', coverData.cover.id + 'c',
     doctorName + 'd',regionData, sexData.sex.id + 'f',typeData.type.id + 'g')
    if (ageData && endTime && coverData.cover.id && doctorName && regionData.length > 0 && sexData.sex.id && typeData.type.id) {
      try {
        await commonApi.userInfoKeep({
          age: ageData,
          province: newRegionData[0],
          city: newRegionData[1],
          doctor_name: doctorName,
          fault_type: typeData.type.id,
          fault_name: typeData.type.name,
          yt_type: coverData.cover.id,
          yt_name: coverData.cover.name,
          sex: sexData.sex.id,
          // total_yt_count: countData,
          planEndDateStr: endTime,
          nick_name: userInfo.nickName
        })
        Taro.navigateTo({ url: '/pages/index/index'})
      } catch(err) {
        Taro.showToast({ title: '生成失败', icon: 'none' })
      }
    } else {
      Taro.showToast({ title: '请补全信息', icon: 'none'})
    }
  }

  deleteAllStr(str: any) {
    let list = str.split('全部')
    return list.join('')
  }

  routerTo(path: string) {
    console.log('进', path)
    Taro.navigateTo({url: `/pages/${path}/${path}`})
  }

  getUserInfo = async (e: any) => {
    const userInfo = e.detail.userInfo
    if (userInfo) {
      this.personalPageGenerate(userInfo)
    } else {
      Taro.showToast({ title: '授权失败', icon: 'none'})
    }
  }

  render () {
    const { coverData, regionData, sexData, typeData, doctorName, countData, ageData, birthday, endTime} = this.state
    return (
      <View className='home-import'>
        <View className='import-bg'></View>
        <View className='import-box'>
          {/* 牙套种类 */}
          <Picker mode='selector'
            range={coverData.list}
            rangeKey='name'
            value={coverData.list.indexOf(coverData.cover)}
            onChange={e => {this.onChange(e, 'coverData')}}>
            <Option title='牙套种类' result={coverData.cover.name}></Option>
          </Picker>
          {/* 所在城市 */}
          <Picker mode='region'
            value={regionData}
            customItem='全部'
            onChange={e => {this.onChange(e, 'regionData')}}>
            <Option title='所在城市' result={regionData.length ? `${(regionData[0] || '')}${regionData[1] || ''}${(regionData[2] ||  '')}` : ''}></Option>
          </Picker>
          {/* 医生名字 */}
          <Option title='医生名字' result={doctorName} type='text' blur={e => {this.onChange(e, 'doctorName')}}></Option>
          {/* 性别 */}
          <Picker mode='selector'
            range={sexData.list}
            rangeKey='name'
            value={sexData.list.indexOf(sexData.sex)}
            onChange={e => {this.onChange(e, 'sexData')}}>
            <Option title='性别' result={sexData.sex.name}></Option>
          </Picker>
          {/* 年龄 */}
          <Option title='年龄' result={ageData} type='number' blur={e => {this.onChange(e, 'ageData')}}></Option>
          {/* 出生日期 */}
          {/* <Picker mode='date'
            value='YYYY-MM-DD'
            onChange={e => {this.onChange(e, 'birthday')}}>
            <Option title='出生日期' result={birthday || ''}></Option>
          </Picker> */}
          {/* 主要类型 */}
          <Picker mode='selector'
            range={typeData.list}
            rangeKey='name'
            value={typeData.list.indexOf(typeData.type)}
            onChange={e => {this.onChange(e, 'typeData')}}>
            <Option title='主要类型' result={typeData.type.name || ''}></Option>
          </Picker>
          {/* 结束时间 */}
          <Picker mode='date'
            value='YYYY-MM-DD'
            onChange={e => {this.onChange(e, 'endTime')}}>
            <Option title='预计结束时间' result={endTime  || ''}></Option>
          </Picker>
          <View className='import-btn'>
            <View className='btn-left' onClick={this.routerTo.bind(this, 'community')}>日志社区</View>
            <Button openType='getUserInfo' onGetUserInfo={this.getUserInfo} className='btn-right'>生成主页<View className='right-icon'></View></Button>
          </View>
        </View>
      </View>
    )
  }
}

export default HomeImport
