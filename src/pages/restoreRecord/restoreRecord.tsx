import React, { Component } from 'react'
import { View, Picker, Input, Image } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'

import { recordApi } from '../../services'
import Option from '../../component/option/option'
import './restoreRecord.scss'

type PageStateProps = {
  store: {
    commonStore: {
      userInfoGet: Function,
      userInfo: {
        ytRecordList: Array<any>,
        recheckRecordList: Array<any>
        wxUser: {
          isUpdated: number,
          city: string
          age: number
          doctor_name: string 
          fault_name: string
          fault_type: number
          yt_name: string
          yt_type: number
          sex: number
          total_yt_count: number,
          province: string,
          planEndDateStr: string
        }
      },
      teethDictData: Array<any>,
      diseaseDictData: Array<any>,
      visitDictData: Array<any>,
    },
    loginStore: {
      loginStatus: Promise<any>
    }
  }
}

interface RestoreRecord {
  props: PageStateProps;
}

enum RecordTyep {
  one =  '地包天',
  two = '天包地',
  three = '包着地'
}

@inject('store')
@observer
class RestoreRecord extends Component {
  public state = {
    recordImage: '',
    inspectFirstDate: '',
    inspectSecondDate: '',
    inspectThirdDate: '',
    inspectCount: 1,
    inspectText: '',
    coverData: {
      cover: { name: '', id: 0 },
      list: [
        { name: '地包天', id: 1 },
        { name: '天包地', id: 2 },
        { name: '包着地', id: 3 },
      ]
    },
    coverFirstData: {
      cover: { name: '', id: 0 },
      list: [
        { name: '地包天', id: 1 },
        { name: '天包地', id: 2 },
        { name: '包着地', id: 3 },
      ]
    },
    restoreRecordId: ''
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    const { store: { commonStore } } = this.props
    const { coverData, coverFirstData} = this.state
    coverData.list = commonStore.visitDictData.slice()
    coverFirstData.list = commonStore.visitDictData.slice()
    if (id) {
      this.setState({ restoreRecordId: id})
      const { data } = await recordApi.reCheckRecordGet({ id })
      const { checkDateStr, beginDateStr, check_feel, check_number, check_type,check_type_name, next_check_type,next_check_type_name, imgUrl, nextCheckDateStr} = data
      this.state.coverFirstData.cover.id = check_type
      this.state.coverFirstData.cover.name = check_type_name
      this.state.coverData.cover.id = next_check_type
      this.state.coverData.cover.name = next_check_type_name
      this.setState({
        inspectFirstDate: checkDateStr,
        inspectCount: check_number,
        coverFirstData: this.state.coverFirstData,
        coverData: this.state.coverData,
        inspectText: check_feel,
        inspectThirdDate: nextCheckDateStr || '',
        inspectSecondDate: beginDateStr,
        recordImage: imgUrl
      })
    }
  }

  recordTypeConversion(type: number) {
    let data = ''
    switch(type) {
      case 1: data = RecordTyep.one; break;
      case 2: data = RecordTyep.two; break;
      case 3: data = RecordTyep.three; break;
    }
    return data
  }

  onChange = (e, type) => {
    let data = this.state[type]
    console.log(data, e.detail.value)
    switch (type) {
      case 'coverFirstData':
        e.detail.value >= 0 && (data.cover = data.list[e.detail.value])
        break
      case 'coverData':
        e.detail.value >= 0 && (data.cover = data.list[e.detail.value])
        break
      case 'inspectCount':
        data = e.detail.value
        break
      case 'restoreRecord':
        data = e.detail.value.split('-').join('/')
        break
      case 'inspectFirstDate':
        data = e.detail.value.split('-').join('/')
        break
        case 'inspectThirdDate':
          data = e.detail.value.split('-').join('/')
          break
      default:
        data = e.detail.value
    }
    this.setState({ [type]: data })
  }

  routerToExamineRecord = async () => {
    const { restoreRecordId, coverData, coverFirstData, inspectCount, inspectFirstDate, inspectThirdDate, inspectText} = this.state
    if (coverData.cover.id && coverFirstData.cover.id && inspectFirstDate &&
    inspectThirdDate  &&inspectText && this.state.recordImage &&
    inspectFirstDate  != '' && inspectThirdDate != ''
    ) {
      console.log(coverData.cover.id , coverFirstData.cover.id , inspectFirstDate ,
        inspectThirdDate  ,inspectText , this.state.recordImage ,
        inspectFirstDate  , inspectThirdDate )
      const { data } = await recordApi[restoreRecordId ? 'reCheckRecordUpdate' : 'reCheckRecordAdd'](Object.assign({
        checkDateStr: inspectFirstDate,
        check_number: inspectCount,
        check_type: coverFirstData.cover.id,
        check_type_name: coverFirstData.cover.name,
        next_check_type: coverData.cover.id,
        next_check_type_name: coverData.cover.name,
        check_feel: inspectText,
        nextCheckDateStr: inspectThirdDate,
        // begin_date: inspectSecondDate,
        imgUrl: this.state.recordImage
      }, restoreRecordId ? { id: restoreRecordId } : {} ))
      Taro.navigateTo({ url: `/pages/examineRecord/examineRecord?id=${data.id}` })
    } else {
      Taro.showToast({ title: '请填写完整信息', icon: 'none' })
    }

  }

  chooseImage = async () => {
    const { tempFilePaths } = await Taro.chooseImage({ count: 1 })
    const { data } = await recordApi.uploadFileImage({ filePath: tempFilePaths[0] })
    this.setState({ recordImage: data})
  }

  render () {
    const { 
      recordImage, 
      inspectCount,
      inspectFirstDate,
      inspectSecondDate, 
      inspectThirdDate, 
      inspectText, 
      coverData, 
      coverFirstData } = this.state
    return (
      <View className='restore-record'>
        {/* 第几次复诊 */}
        <Option title='第几次复诊' result={inspectCount} type='text' blur={e => {this.onChange(e, 'inspectCount')}}></Option>
        {/* 复诊日期 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'inspectFirstDate')}}>
          <Option title='复诊日期' result={inspectFirstDate}></Option>
        </Picker>
        {/* 这次复诊做了啥 */}
        <Picker mode='selector'
          range={coverFirstData.list}
          rangeKey='name'
          value={coverFirstData.list.indexOf(coverFirstData.cover)}
          onChange={e => {this.onChange(e, 'coverFirstData')}}>
          <Option title='这次复诊做了啥' result={coverFirstData.cover.name}></Option>
        </Picker>
        {/* 记录下这副牙套的心里感受 */}
        <View className='notes-box'>
          <Input className='notes' placeholder='记录下这次复诊的心情...' value={inspectText} onBlur={e => {this.onChange(e, 'inspectText')}}></Input>
        </View>
        {/* 上传图片 */}
        <View className='upload-file-box'>
          {
            recordImage && <View className='image-box'>
              {/* <View className='image-delete'></View> */}
              <Image src={recordImage} className='file-image'></Image>
            </View>
          }
          {
            recordImage ? <View className='edit-file-icon' onClick={this.chooseImage}>修改</View> : 
            <View className='add-file-icon' onClick={this.chooseImage}></View>
          }
        </View>
        {/* 什么时候开始初带 */}
        {/* <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'inspectSecondDate')}}>
          <Option title='什么时候开始初带' result={inspectSecondDate}></Option>
        </Picker> */}
        {/* 下一次复诊时间 */}
        <Picker mode='date'
          value='YYYY-MM-DD'
          onChange={e => {this.onChange(e, 'inspectThirdDate')}}>
          <Option title='下一次复诊时间' result={inspectThirdDate}></Option>
        </Picker>
        {/* 下一次复诊要做啥 */}
        <Picker mode='selector'
          range={coverData.list}
          rangeKey='name'
          value={coverData.list.indexOf(coverData.cover)}
          onChange={e => {this.onChange(e, 'coverData')}}>
          <Option title='下一次复诊要做啥' result={coverData.cover.name}></Option>
        </Picker>

        <View className='btn-box'>
          <View className='braces-btn' onClick={this.routerToExamineRecord}>
            下一步<View className='btn-icon'></View>
          </View>
        </View>
      </View>
    )
  }
}

export default RestoreRecord
