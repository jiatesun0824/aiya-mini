import React, { Component } from 'react'
import { View, Picker, Text, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance }  from '@tarojs/taro'


import './community.scss'
import { recordApi } from '../../services'



type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Community { props: IProps }

interface IState {
  dailyRecords: Array<any>
  regionData: Array<any>
}

@inject('store')
@observer
class Community extends Component {
  public state: IState = {
    regionData: [],
    dailyRecords: []
  }

  componentWillMount () { 
    this.init()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  init() {
    this.getDailyRecords()
  }

  getDailyRecords =  async (item?: any) => {
    const { data } = await recordApi.getDailyRecords(item || {})
    this.setState({ dailyRecords: data })
  }

  onChange = (e: any, type: string) => {
    const list = e.detail.value.slice().map((item: any) => item == '全部' ? '' : item)
    this.setState({ [type]: e.detail.value })
    this.getDailyRecords({ province: list[0], city: list[1] })
  }

  setRegionData = (regionData) => {
    let str = ''
    for (let i = 0, len = regionData.length; i < len; i++) {
      str = str + regionData[i]
      if (regionData[i] === '全部') break
    }
    console.log(str)
    return str
  } 

  routerTo(path :string, item: any) {
    Taro.navigateTo({ url: `/pages/${path}/${path}?id=${item.open_id}` })
  }

  render () {
    const { regionData, dailyRecords } = this.state
    return (
      <View className='community'>
        <View className='selector-box'>
          <Picker mode='region'
            value={regionData}
            customItem='全部'
            onChange={e => {this.onChange(e, 'regionData')}}>
              <View className='community-area'>
                {regionData.length ? this.setRegionData(regionData) : '请选择'}
                <View className='area-icon'></View>
              </View>
          </Picker>
        </View>
        <View className='community-list'>
          {
            dailyRecords.map((item, index) => {
              return <View className='community-item' key={index} onClick={this.routerTo.bind(this, 'bracesLog', item)}>
                <Image src={item.imgUrl} className='item-image'></Image>
                <View className='item-title'>{item.doctorname}</View>
                <View className='item-name'>
                  {/* <Image src='' className='head'></Image> */}
                  {item.ytName}
                </View>
              </View>
            })
          }
        </View>
      </View>
    )
  }
}

export default Community
