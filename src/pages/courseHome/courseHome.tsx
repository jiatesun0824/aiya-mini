import React, { Component } from 'react'
import { View} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import { recordApi } from '../../services'

import './courseHome.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface CourseHome { props: IProps }

@inject('store')
@observer
class CourseHome extends Component {
  public state = {
    feelContent: '',
    routeType: 0
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { type } = router?.params!
    const { data } = await recordApi.mindRecordGet()
    const { feel_content } = data
    this.setState({ feelContent: feel_content, routeType: type})
  }

  navigateBack = () => {
    this.state.routeType == 0 ? Taro.navigateTo({ url: '/pages/diaryCorrect/diaryCorrect?type=1'}) : Taro.navigateBack()
  }

  render () {
    const { feelContent } = this.state
    console.log(feelContent)
    return (
      <View className='course-home'>
        <View className='course-card'>
          <View className='card-title'>
            <View className='title-name'>心路历程</View>
            <View className='title-line'></View>
          </View>
          <View className='card-content'>
            { feelContent }
          </View>
        </View>
        <View className='btn-box'>
          <View className='btn' onClick={this.navigateBack}>修正信息<View className='btn-icon'></View></View>
        </View>
        {/* <View className='btn' onClick={this.navigateBack}>
          修正信息 <View className='btn-icon'></View>
        </View> */}
      </View>
    )
  }
}

export default CourseHome
