import React, { Component } from 'react'
import { View, Text, Image } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import classNames from 'classNames'
import Taro from '@tarojs/taro'

import './index.scss'
// import { recordApi } from '../../services'

type PageStateProps = {
  store: {
    commonStore: {
      userInfoGet: Function,
      userInfo: {
        ytRecordList: Array<any>,
        recheckRecordList: Array<any>
        wxUser: {
          isUpdated: number
        },
        title: string
      },
      mindRecordList: Array<any>
    },
    loginStore: {
      loginStatus: Promise<any>
    }
  }
}

interface Index {
  props: PageStateProps;
}

interface IState {
  tabList: Array<any>,
  bracesInfo: Array<any>,
  restoreRecord: Array<any>,
  bracesInfoActiveIndex: number,
  restoreRecordActiveIndex: number,
  mindRecordList: Array<any>
  bracesTitle: number
}

@inject('store')
@observer
class Index extends Component {
  public state: IState = {
    tabList: [
      { name: '当地牙套群', icon: 'tab_group_icon', url: 'addGroup' },
      { name: '医生主页', icon: 'tab_doctor_icon', url: 'doctorHome' },
      { name: '矫正心路', icon: 'tab_correct_icon', url: 'diaryCorrect', secondUrl: 'courseHome' },
      { name: '我的日记', icon: 'tab_diary_icon', url: 'bracesLog' },
      { name: '我的积分', icon: 'tab_integral_icon', url: 'scorecard' },
      { name: '修改信息', icon: 'tab_info_icon', url: 'homeImport' },
    ],
    bracesInfo: [],
    restoreRecord: [],
    bracesInfoActiveIndex: 0,
    restoreRecordActiveIndex: 0,
    mindRecordList: [],
    bracesTitle: 0
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidHide () { }

  componentDidShow () { 
    this.showInit()
  }

  showInit = async() => {
    const { store: { commonStore, loginStore } } = this.props
    const flag = await loginStore.loginStatus
    if (flag) {
      await commonStore.userInfoGet()
      if (commonStore.userInfo.wxUser.isUpdated=== 1) {
        this.setState({ bracesInfo: commonStore.userInfo.ytRecordList, restoreRecord:  commonStore.userInfo.recheckRecordList,
          bracesTitle: commonStore.userInfo.title
         })
      } else {
        Taro.redirectTo({ url: '/pages/homeImport/homeImport' })
      }
    }
  }

  selectBracesInfo = (type: string, index: number, item: any) => {
    const { id } = item
    this.setState({[type + 'ActiveIndex']: index})
    Taro.navigateTo({ url: `/pages/${type}/${type}?id=${id}` })
  }

  addRestorecord = (type: string) => {
    console.log(type)
    Taro.navigateTo({ url: `/pages/${type}/${type}` })
  }

  tabHandle = (item: any) => {
    let { url, secondUrl } = item
    const { store: { commonStore } } = this.props
    url === 'diaryCorrect' && commonStore.mindRecordList.length > 0 && (url = secondUrl)
    Taro.navigateTo({ url: `/pages/${url}/${url}?type=0` })
  }

  bracesGroupJoin() {
    Taro.navigateTo({ url: '/pages/addGroup/addGroup'})
  }

  doctorPageToGo() {
    Taro.navigateTo({ url: '/pages/doctorHome/doctorHome'})
  }

  informationModify() {
    Taro.navigateTo({ url: '/pages/homeImport/homeImport'})
  }

  routerToJourney() {
    Taro.navigateTo({ url: '/pages/diaryCorrect/diaryCorrect'})
  }

  render () {
    const { tabList, bracesInfo, restoreRecord, bracesTitle } = this.state
    return (
      <View className='home'>
        <View className='home-tab'>
          <View className='tab-title'>
            <View className='title-line'></View>
              我已经佩戴牙套<Text className='title-day'>{bracesTitle}</Text>天
            <View className='title-line'></View>
            <View></View>
          </View>
          <View className='tab-box'>
            {
              tabList.map((item, index) => {
                return <View className='tab-item'
                  onClick={ this.tabHandle.bind(this, item) }
                  key={index}>
                  <View className={classNames('tab-icon', item.icon)}></View>
                  <View className='tab-name'>{item.name}</View>
                </View>
              })
            }
          </View>
        </View>
        {/* 点击页卡编辑复诊日信息 */}
        <View className='home-box'>
          <View className='box-title' onClick={this.addRestorecord.bind(this, 'restoreRecord')}>
            <View>{`${restoreRecord.length > 0 ? '编辑复诊日信息' : '点击新增复诊日信息'}`}</View>
            <View className='add-btn'>
              添加<View className='add-icon'></View>
            </View>
          </View>
            {
              restoreRecord.length > 0 && <View className='box-select'>
              {
                restoreRecord.map((item, index) => {
                  return <View className='select-item'
                    key={index} 
                    onClick={() => {this.selectBracesInfo('restoreRecord', index, item)}}>
                    <View>{item.check_number + '、'} {item.recordContent}</View>
                    <View className='select-icon'></View>
                  </View>
                })
              }
           </View>
            }
        </View>
                {/* 点击页卡编辑该副牙套信息 */}
        <View className='home-box'>
          <View className='box-title' onClick={this.addRestorecord.bind(this, 'bracesInfo')}>
            <View>{`${bracesInfo.length > 0 ? '编辑该副牙套信息' : '点击新增牙套信息'}`}</View>
            <View className='add-btn'>
              添加<View className='add-icon'></View>
            </View>
          </View>
          {
            bracesInfo.length > 0 && <View className='box-select'>
              {
                bracesInfo.map((item, index) => {
                  return <View className='select-item'
                    key={index} 
                    onClick={() => {this.selectBracesInfo('bracesInfo', index, item)}}>
                    <View className='select-message'>{item.yt_number + '、'}{item.ytDetailInfo}</View>
                    <View className='select-icon'></View>
                  </View>
                })
              }
            </View>
          }
        </View>
      </View>
    )
  }
}

export default Index
