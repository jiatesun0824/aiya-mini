import React, { Component } from 'react'
import { View, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import { doctorApi } from '../../services'
import Taro, { getCurrentInstance } from '@tarojs/taro'
import { recordApi } from '../../services'

import './doctorHome.scss'

type PageStateProps = {
  store: {
    commonStore: {
      userInfo: {
        wxUser: {
          doctor_name: string
        }
      }
    },
  }
}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface DoctorHome { props: IProps }

interface IState  {
  doctorInfo: any,
  doctorCaseList: Array<any>
  doctorCommentList: Array<any>
}

@inject('store')
@observer
class DoctorHome extends Component {
  public state: IState = {
    doctorInfo: {},
    doctorCaseList: [],
    doctorCommentList: []
  }

  componentWillMount() { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit() {
    this.doctorInfoGet()
  }

  doctorInfoGet =  async () => {
    const { router } = getCurrentInstance()
    const { name } = router?.params!
    const { store: { commonStore }}  = this.props
    const doctorName: string = commonStore.userInfo.wxUser.doctor_name
    const { data } = await doctorApi.doctorsListGet({ name: name || doctorName })
    if (data.length === 0) {
      Taro.showToast({ title: '暂无此医生信息', duration: 2000, mask: true, icon: 'none'})
      setTimeout(() => Taro.redirectTo({ url: '/pages/doctorList/doctorList'}), 2000)
    } else {
      const { id } = data[0]
      console.log(data)
      const res = await doctorApi.doctorsCaseListGet({ doctorId: id})
      this.setState({ doctorInfo: data[0], doctorCaseList: res.data})
      const comments = await doctorApi.doctorsCommentsListGet({ doctorId: id })
      this.setState({ doctorCommentList: comments.data})
    }
  }

  chooseImage = async () => {
    try {
      const { tempFilePaths } = await Taro.chooseImage({ count: 1 })
      const { data } = await recordApi.uploadFileImage({ filePath: tempFilePaths[0] })
      doctorApi.doctorsCommentAdd({ img_1: data, doctors_id : this.state.doctorInfo.id})
      this.doctorInfoGet()
    } catch(err) {}
  }

  previewImage = (url: string) => {
    Taro.previewImage({ urls: [url], current: url})
  }

  render () {
    const { photo, is_cos, edu_1, name, technology } = this.state.doctorInfo
    const { doctorCaseList, doctorCommentList } = this.state
    return (
      <View className='doctorHome'>
        <View className='header'>
          <View className='header-title'>
            <Image className='header-head' src={photo} />
            <View className='header-info'>
              <View className='header-name'>{name}</View>
              {/* <View className='header-explain'>{is_cos === 1 ? 'cos会员 | ' : '非cos会员'}</View> */}
            </View>
          </View>
          <View className='header-presentation'>
            简介：{technology}
          </View>
        </View>
        <View className='case-box'>
          <View className='case-title'>医生案例</View>
          <View className='case-list'>
            {
              doctorCaseList.map((item) => 
                <View className='case-item' onClick={this.previewImage.bind(this, item.img_1)}>
                  <Image src={item.img_1} className='item-image'></Image>
                </View>
              )
            }
          </View>
        </View>
        <View className='case-box'>
          <View className='case-title'>患者评价</View>
          <View className='case-list'>
            {
              doctorCommentList.map((item) => 
                // <View className='case-item-comment'>
                //   <View className='item-comment-time'>{item.createDateStr}</View>
                //   <Text className='item-comment-message'>{item.comments}</Text>
                // </View>
                <View className='case-item' onClick={this.previewImage.bind(this, item.img_1)}>
                  <Image src={item.img_1} className='item-image'></Image>
                </View>
              )
            }
          </View>
        </View>
        <View className='btn' onClick={this.chooseImage}>
          发表评论
          <View className='btn-icon'></View>
        </View>
      </View>
    )
  }
}

export default DoctorHome
