import React, { Component } from 'react'
import { View, Text, Image} from '@tarojs/components'
import { observer, inject } from 'mobx-react'

import './addGroup.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface AddGroup { props: IProps }

@inject('store')
@observer
class AddGroup extends Component {
  public state = {}

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    
    return (
      <View className='add-group'>
        <View className='card-box'>
          <View className='card-title'>扫码添加<Text className='title-color'>小优医生</Text>为好友</View>
          <View className='card-explain'>经审核后可以加入当地牙套交流群</View>
          <Image src='https://aiyouya.oss-cn-shenzhen.aliyuncs.com/wechat/user/20201122153337.jpg' className='code-image'></Image>
        </View>
        <View className='hint'>如果您所在的地区，还未建立牙套群，为您重新建立，逐步吸引</View>
      </View>
    )
  }
}

export default AddGroup
