import React, { Component } from 'react'
import { View, Image, Video } from '@tarojs/components'
import { observer, inject } from 'mobx-react'
import { commonApi, recordApi } from '../../services'
import { getCurrentInstance } from '@tarojs/taro'

import './explain.scss'

type PageStateProps = {}

/**
 * 初始化actions
*/
type PageDispatchProps = {}

type PageOwnProps = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Explain { props: IProps }

interface IState {
  publicScienceList: Array<any>
  publicScience: any
}

@inject('store')
@observer
class Explain extends Component {
  public state: IState = {
    publicScienceList: [],
    publicScience: {}
  }

  componentWillMount () { 
    this.willMountInit()
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  willMountInit = async () => {
    const { router } = getCurrentInstance()
    const { id } = router?.params!
    this.getPublicScience(id as string)
  }

  getPublicScience =  async (id: string) => {
    const { data } = await recordApi.getPublicScienceByType({ id })
    this.setState({publicScience: data})
  }

  render () {
    const { publicScience } = this.state
    
    return (
      <View className='explain'>
        {
            <View className='content-box'>
              <View className='content-title'>{publicScience.checkName}</View>
              <View className='content-message'>{publicScience.desciption}</View>
              <View className='content-title'>{publicScience.img1_desc}</View>
              {
                publicScience.img1 && <Image  src={publicScience.img1} className='content-image'/>
              }
              <View className='content-title'>{publicScience.img2_desc}</View>
              {
                publicScience.img2 && <Image  src={publicScience.img2} className='content-image'/>
              }
              {
                publicScience.videoImg && <Video src={publicScience.videoImg} autoplay={false} className='content-video'/>
              }
            </View>
        }
      </View>
    )
  }
}

export default Explain
