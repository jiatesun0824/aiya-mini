import React, { Component } from 'react'
import { Provider } from 'mobx-react'
import Taro from '@tarojs/taro'


import store from './store'

import './app.scss'
import { commonApi } from './services'

class App extends Component {

  public globalData = {
    loginStatus: new Promise(() => { })
  }

  componentWillMount () { 
    this.init()
  }

  componentDidMount () {
  }

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  init() {
     store.loginStore.loginStatus = this.login()
  }

  login(): Promise<any> {
    return new Promise((resolve) => {
      Taro.login({
        success: async (res) => {
          if (res.code) {
            await store.loginStore.login(res.code)
            resolve(true)
            this.getBasicType()
            await store.commonStore.mindRecordListGet()
          }
        },
        fail: () => {
          resolve(false)
        }
      })
    })
  }

  getBasicType = async () => {
     store.commonStore.getBasicType()
  }

  // this.props.children 就是要渲染的页面
  render () {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}

export default App
