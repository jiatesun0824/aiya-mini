import { observable } from 'mobx'
import { commonApi, recordApi } from '../../services'

const commonStore = observable({
  userInfo: {},
  teethDictData: [],
  diseaseDictData: [],
  visitDictData: [],
  mindRecordList: [],
  async userInfoGet() {
    const { data } = await commonApi.userInfoGet()
    this.userInfo = data
  },
  async getBasicType() {
    ['teethDictData', 'diseaseDictData', 'visitDictData'].forEach( async (item: string, index: number) => {
      const { data } = await commonApi.getBasicType({type: index})
      this[item] = data.map((item: any) => ({name: item.value,id: item.id, }))
    })
  },
  async mindRecordListGet() {
    const { data } = await recordApi.mindRecordListGet()
    this.mindRecordList = data
  }
})

export default commonStore
