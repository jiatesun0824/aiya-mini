import { observable } from 'mobx'
import { getToken, setToken } from '../../utils/auth'
import { login } from '../../services'

const loginStore = observable({
  token: getToken(),
  loginStatus: new Promise(() => {}),
  async login(code: string) {
    const { data } = await login.login({ code })
    const { token } = data
    this.token = setToken(token)
    
  },
})

export default loginStore