import loginStore from './login'
import commonStore from './common'

export default {
  loginStore,
  commonStore
}