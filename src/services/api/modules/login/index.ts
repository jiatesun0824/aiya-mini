import { request } from '../../../request'

export default {
  login(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/login', params)
  },
  publicScienceGet(params: PlainObject): Promise<any> {
    return request('baseUrl').get('/api/user/getPublicScience', params)
  }
}