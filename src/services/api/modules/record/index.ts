import { request } from '../../../request'

interface IUploadData {
  filePath: string
}

export default {
  reCheckRecordAdd(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/addReCheckRecord', params)
  },
  reCheckRecordGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getRecheckById/${params.id}`, {})
  },
  reCheckRecordUpdate(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/updateReCheckRecord`, params)
  },
  ytRecordAdd(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/addYTRecord`, params)
  },
  ytRecordGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getYtRecordById/${params.id}`, {})
  },
  ytRecordUpdate(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/updateYTRecord`, params)
  },
  mindRecordAdd(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/addMindRecord`, params)
  },
  mindRecordGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getMindRecordById/${params.id}`, {})
  },
  mindRecordListGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/getMindRecordList`, params)
  },
  mindRecordUpdate(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/updateMindRecord`, params)
  },
  otherPersonMindRecordsGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/getOtherPersonMindRecords`, params)
  },
  uploadFileImage(params: IUploadData = { filePath: ''}): Promise<any> {
    return request('baseUrl').upload('/api/user/upload', params)
  },
  // 牙套日记详情
  getMyDailyRecord(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getMyDailyRecord`, params)
  },
  getDailyRecords(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/getDailyRecords`, params)
  },
  getYTRecordList(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post(`/api/user/getYTRecordList`, params)
  },
  // 允许分享牙套
  allowShare(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/allowShare/${params.shareType}`, {})
  },
  getPublicScienceByType(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getPublicScienceByType/${params.id}`, {})
  },
}