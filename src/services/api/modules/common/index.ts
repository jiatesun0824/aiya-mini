import { request } from '../../../request'

export default {
  userInfoKeep(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/saveBasicInfo', params)
  },
  userInfoGet(params: PlainObject = {}): Promise<any> {    
    return request('baseUrl').post('/api/user/getCurrentUserDetail', params)
  },
  publicScienceGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/getPublicScience', params)
  },
  getBasicType(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').get(`/api/user/getBasicType/${params.type}`, {})
  },
}