import { request } from '../../../request'

export default {
  doctorsListGet(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/getDoctorsList', params)
  },
  doctorsCaseListGet(params: PlainObject = {}): Promise<any>  {
    return request('baseUrl').get(`/api/user/getDoctorsCaseList/${params.doctorId}`, {})
  },
  doctorsCommentsListGet(params: PlainObject = {}): Promise<any>  {
    return request('baseUrl').get(`/api/user/getDoctorsCommentsList/${params.doctorId}`, {})
  },
  doctorsCommentAdd(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/addDoctorsComment', params)
  },
  getDoctorsList(params: PlainObject = {}): Promise<any> {
    return request('baseUrl').post('/api/user/getDoctorsList', params)
  }
}