import Taro from '@tarojs/taro'

import baseUrl from './api/domain_name'

import { getToken } from '../utils/auth'

interface IRequestParams {
  url: string
  data: object
  method:  "OPTIONS" | "GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "TRACE" | "CONNECT" | undefined
  contentType: string
  base: string
}

interface IUploadData {
  filePath: string
}

interface IUploadParams {
  url: string
  data: IUploadData
  base: string
}

interface Data {
  code: number
  data: string
  message: string
}

interface Response {
  data: Data
}

const requestFn = (params: IRequestParams): Promise<any> => {
  let {url, data, method, contentType, base} = params
  Taro.showLoading({ title: '加载中', mask: true })
  return new Promise((resolve, reject) => {
    Taro.request({
      url: baseUrl[base] + url,
      data,
      method,
      header: { 
        'content-type': contentType,
        'token': getToken(),
        'Authorization': getToken()
      }
    }).then(async (res: Response) => {
      Taro.hideLoading()
      resolve(res.data)
    }).catch((err: any) => { 
      Taro.hideLoading()
      reject(err)
    })
  })
}

const uploadFn = (params: IUploadParams): Promise<any> => {
  let {url, data, base} = params
  Taro.showLoading({ title: '加载中', mask: true })
  return new Promise((resolve, reject) => {
    Taro.uploadFile({
      name: 'file',
      url: baseUrl[base] + url,
      filePath: data.filePath,
      header: { 
        'token': getToken(),
        'Authorization': getToken()
      }
    }).then(async (res: any) => {
      Taro.hideLoading()
      resolve(JSON.parse(res.data))
    }).catch((err: any) => { 
      Taro.hideLoading()
      reject(err)
    })
  })
}

export const request = (base: string) => {
  return {
    get(url: string, data: object) {
      return requestFn({
        url,
        base,
        data,
        method: 'GET',
        contentType: 'application/json'
      })
    },
    post(url: string, data: object) {
      return requestFn({
        url,
        base,
        data,
        method: 'POST',
        contentType: 'application/json'
      })
    },
    upload(url: string, data: IUploadData) {
      return uploadFn({
        url,
        base,
        data,
      })
    }
  }
}
