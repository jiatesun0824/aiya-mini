export {default as login} from './api/modules/login/index'

export {default as commonApi} from './api/modules/common/index'

export {default as doctorApi} from './api/modules/doctor/index'

export {default as recordApi} from './api/modules/record/index'


