import React, { Component } from 'react'
import { View, Input } from '@tarojs/components'
import classNames from 'classNames'
import './option.scss'

type PageStateProps = {
  title: string,
  result?: string | number,
  type?: 'number' | 'text' | 'idcard' | 'digit' | undefined,
  blur?: any,
}

interface Option {
  props: PageStateProps;
}

class Option extends Component {
  public state = {
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const { title, result, type = '' } = this.props
    return (
      <View className='selector-box'>
        <View className='selector-result'>
          <View className='result-left'>{title}</View>
          {
            type !== ''
            ? <Input type={type} placeholder='请输入' value={result} onBlur={e => {this.props.blur(e)}}></Input>
            : <View className='result-right'>
              <View className={classNames('result-name', !result && 'text-color')}>{result || '请选择'}</View>
              <View className='result-icon'></View>
            </View>
          }
        </View>
        <View className='line'></View>
      </View>
    )
  }
}

export default Option
