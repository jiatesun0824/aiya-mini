export default {
  pages: [
    'pages/index/index', // 首页
    'pages/homeImport/homeImport',// 首页基本信息输入
    'pages/bracesInfo/bracesInfo', // 单副牙套信息
    'pages/bracesShare/bracesShare', // 单副牙套分享
    'pages/doctorHome/doctorHome', // 医生主页
    'pages/doctorList/doctorList', // 医生主页列表
    'pages/examineRecord/examineRecord', // 单次复诊记录
    'pages/restoreRecord/restoreRecord', // 单次修复记录
    'pages/courseHome/courseHome', // 心理历程主页
    'pages/diaryCorrect/diaryCorrect', // 矫正心理历程
    'pages/explain/explain', // 科普信息
    'pages/otherCourse/otherCourse',// 看别人的矫正历程
    'pages/addGroup/addGroup', // 加群
    'pages/community/community', // 牙套日志社区
    'pages/bracesLog/bracesLog', // 牙套日志
    'pages/scorecard/scorecard', // 积分
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
  }
}
